<?

class FigmaInsert
{
    public static function wfFigmaSetup(&$parser)
    {
        $parser->setHook('figma', array('FigmaInsert', 'wfFigmaRender'));
        return true;
    }

    public static function wfFigmaRender($input, array $args, Parser $parser, PPFrame $frame)
    {
        $error = array();

        $fileName = '';
        if (str_replace(' ', '', $input) == '')
            array_push($error, 'Set a Figma file name');
        else
            $fileName = $input;

        $node = (isset($args['node'])) ? '?node='.$args['node'] : '';

        $out = '';
        if (empty($error)) {
            $out .= '<div style="width: 400px; height: 400px; resize: both; overflow: auto;">';
            $out .= '<iframe ' .
                'height="100%" ' .
                'width="100%" ' .
                'src="https://www.figma.com/embed?embed_host=astra&url=' .
                'https://www.figma.com/file/LKQ4FJ4bTnCSjedbRpk931/' . $fileName . $node . '" ' .
                'allowfullscreen ' .
                '/>';
            $out .= '</div>';
        } else {
            foreach ($error as $errorMsg)
                $out .= $errorMsg."\n";
        }

        return $out;
    }
}
?>